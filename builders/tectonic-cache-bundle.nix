{ pkgs ? import <nixpkgs> { }
, stdenvNoCC ? pkgs.stdenvNoCC
, cacert ? pkgs.cacert
, tectonic ? pkgs.tectonic
, pname ? "tectonic-cache-bundle"
, src ? ./.
, version ? "1"
, outputHash ? builtins.readFile (src + "/tectonicCache-outputHash.txt")
, ...
}:

# this is both a cache and a bundle with bundled resource symlinked to the cache
# https://github.com/tectonic-typesetting/tectonic/issues/685#issuecomment-775752733
# https://github.com/tectonic-typesetting/tectonic-texlive-bundles/tree/master/scripts
stdenvNoCC.mkDerivation {

  inherit pname src version outputHash;

  outputHashAlgo = "sha256";
  outputHashMode = "recursive";

  nativeBuildInputs = [
    tectonic
    cacert
  ];

  unpackCmd = "cp -r --no-preserve=mode $curSrc .";

  dontConfigure = true;

  HOME = ".";

  buildPhase = ''
    runHook preBuild
    tectonic -X build
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    mv .cache/Tectonic $out
    cd $out
    ln -vs $(ls urls/* | head -1) $out/SHA256SUM # bundle checksum; for a fresh cache, there should be just one url
    ln -vs $(ls redirects/*.txt | head -1) $out/URL # bundle url; for a fresh cache, there should be just one url
    IFS=$'\n'
    for I in $(cat $out/manifests/*.txt); do
      FILE=''${I%% *}
      HASH=''${I##* }
      ln -vs files/''${HASH:0:2}/''${HASH:2} $out/$FILE
    done
    runHook postInstall
  '';

}
