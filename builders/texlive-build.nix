{ pkgs ? import <nixpkgs> { }
, lib ? pkgs.lib
, stdenvNoCC ? pkgs.stdenvNoCC
, texlive ? pkgs.texlive
, pname ? "texlive-build"
, src ? ./.
, tectonicTomlFile ? if src == null then null else src + "/Tectonic.toml"
, texlivePkgsFile ? if src == null then null else src + "/texlive-pkgs.nix"
, version ? "1"
, self ? { } # required to get self.lastModified
, ...
}:

let

  texliveCustomPkgs = if texlivePkgsFile == null then { } else import texlivePkgsFile texlive;

  texliveCombined = texlive.combine ({
    inherit (texlive) # for provided packages see https://github.com/NixOS/nixpkgs/blob/master/pkgs/tools/typesetting/tex/texlive/pkgs.nix
      scheme-basic# that is collection-basic and collection-latex
      collection-fontsrecommended# CM fonts, etc.
      collection-langenglish
      latexmk;
  } // texliveCustomPkgs);

  tectonicToml = lib.trivial.importTOML tectonicTomlFile;

  # for reproducible result, it is necessary to set SOURCE_DATE_EPOCH (used by latex to get \today) and PDF Trailer ID:
  # % pdfTeX
  # \pdftrailerid{}
  # % LuaTeX
  # \pdfvariable suppressoptionalinfo 512\relax
  # % XeTeX
  # \special{pdf:trailerid [ <00112233445566778899aabbccddeeff> <00112233445566778899aabbccddeeff> ]}
  # % see https://flyx.org/nix-flakes-latex/
  latexmkForOutput = doc: output:
    let
      # https://tectonic-typesetting.github.io/book/latest/ref/tectonic-toml.html
      jobName = output.name;
      generate = (output.type or "pdf") + {
        latex = "latex"; # This sets the generation of pdf files by pdflatex, and turns off the generation of dvi and ps files.
        lualatex = "lua"; # Generate pdf version of document using lualatex.
        xelatex = "xe"; # Generate pdf version of document using xelatex.
      }."${output.tex_format or "latex"}";
      outputDirectory = "../build/" + jobName; # relative to ./src/
      pretexReproducible = "\\pdftrailerid{}";
      pretexPreamble = if output ? preamble then "\\input{" + output.preamble + "}" else "";
      shellEscape = if (output ? shell_escape) && output.shell_escape then "--shell-escape" else "";
      texFile = "./src/" + (output.index or "index.tex");
    in
    assert ! (output ? postamble); # mklatex supports just pre-tex, not post-tex code
    # https://personal.psu.edu/~jcc8/software/latexmk/latexmk-477.txt
    # -cd: Change to the directory containing the main source file before processing it.
    # -jobname=STRING: Set the basename of output files(s) to STRING, instead of the default, which is the basename of the specified TeX file.
    # -output-directory=FOO: Sets the directory for the output files of *latex.
    # -pretex=CODE: Given that CODE is some TeX code, this options sets that code to be executed before inputting source file.
    # -usepretex: Sets the command lines for latex, etc, so that they use the code set by the option -pretex=CODE
    ''
      latexmk \
        -cd \
        -jobname=${lib.strings.escapeShellArg jobName} \
        -output-directory=${lib.strings.escapeShellArg outputDirectory} \
        ${lib.strings.escapeShellArg ("-" + generate)} \
        -pretex=${lib.strings.escapeShellArg (pretexReproducible + pretexPreamble)} -usepretex \
        -interaction=nonstopmode ${shellEscape} \
        ${texFile}
    '';

in
stdenvNoCC.mkDerivation {

  inherit pname src version;

  nativeBuildInputs = [
    texliveCombined
  ];

  unpackCmd = "cp -r --no-preserve=mode $curSrc .";

  TEXMFHOME = "$TMPDIR/cache";
  TEXMFVAR = "$TMPDIR/cache/var";
  SOURCE_DATE_EPOCH = self.lastModified or 0;

  dontConfigure = true;

  buildPhase = ''
    runHook preBuild
  '' + (lib.strings.concatMapStrings (latexmkForOutput tectonicToml.doc) tectonicToml.output) + ''
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out
    for I in build/*/*.pdf; do
      SUFFIX=_$(basename $I .pdf)
      [ "$SUFFIX" = "_index" ] && unset SUFFIX
      mv -v $I $out/${pname}$SUFFIX.pdf
    done
    runHook postInstall
  '';
}

