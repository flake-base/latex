{ pkgs ? import <nixpkgs> { }
, callPackage ? pkgs.callPackage
, stdenvNoCC ? pkgs.stdenvNoCC
, makeFontsConf ? pkgs.makeFontsConf
, tectonic ? pkgs.tectonic
, pname ? "tectonic-build"
, src ? ./.
, version ? "1"
, ...
}:

let

  tectonicCacheBundle = callPackage ./tectonic-cache-bundle.nix {
    inherit src;
  };

  # prevent Fontconfig error: Cannot load default config file: No such file: (null)
  # ignore warning: failed to load external entity "urn:fontconfig:fonts.dtd"
  fontsConf = makeFontsConf {
    fontDirectories = [ tectonicCacheBundle.outPath ];
  };

in
stdenvNoCC.mkDerivation {

  inherit pname src version;

  nativeBuildInputs = [
    tectonic
    tectonicCacheBundle
  ];

  unpackCmd = "cp -r --no-preserve=mode $curSrc .";

  HOME = ".";
  FONTCONFIG_FILE = fontsConf;

  patchPhase = ''
    runHook prePatch
    # substitute URL for tectonicCacheBundle pkg, however, it wont work as it is not a tectonic bundle
    substituteInPlace Tectonic.toml --replace "'$(head -1 ${tectonicCacheBundle}/URL)'" "'file://${tectonicCacheBundle}'"
    # an alternative: symlink the tectonic cache
    #mkdir -p $HOME/.cache && ln -vs ${tectonicCacheBundle} $HOME/.cache/Tectonic
    runHook postPatch
  '';

  dontConfigure = true;

  buildPhase = ''
    runHook preBuild
    tectonic -X build --print
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p $out
    for I in build/*/*.pdf; do
      SUFFIX=_$(basename $I .pdf)
      [ "$SUFFIX" = "_index" ] && unset SUFFIX
      mv -v $I $out/${pname}$SUFFIX.pdf
    done
    runHook postInstall
  '';
}
