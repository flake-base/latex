{

  description = "Nix flake for building LaTeX documents.";

  nixConfig = {
    # https://nixos.org/manual/nix/unstable/command-ref/conf-file.html
    bash-prompt-suffix = "dev-shell# ";
  };

  inputs = {

    nixpkgs.url = "nixpkgs/release-23.11";

    utils.url = "github:numtide/flake-utils";

  };

  outputs = { self, nixpkgs, utils, ... } @ args:
    let

      defaultSystems =
        let
          excludedSystems = [
            "i686-linux" # dotnet-sdk required by pre-commit-hooks is not available there
          ];
        in
        nixpkgs.lib.lists.subtractLists excludedSystems utils.lib.defaultSystems;

      mkOutputsWithAdditionalInputs = mkOutputsWithInputs: additionalInputs: mkOutputsWithInputs (args // additionalInputs);
      mkOutputs = mkOutputsWithInputs: mkOutputsWithInputs args;

      mkLatexOutputsWithInputs = import ./outputs/texlive.nix self;
      mkLatexOutputsWithAdditionalInputs = mkOutputsWithAdditionalInputs mkLatexOutputsWithInputs;
      mkLatexOutputs = mkOutputs mkLatexOutputsWithInputs;

      mkTectonicOutputsWithInputs = import ./outputs/tectonic.nix self;
      mkTectonicOutputsWithAdditionalInputs = mkOutputsWithAdditionalInputs mkTectonicOutputsWithInputs;
      mkTectonicOutputs = mkOutputs mkTectonicOutputsWithInputs;

    in
    {

      lib = {
        inherit defaultSystems
          mkLatexOutputsWithInputs mkLatexOutputsWithAdditionalInputs mkLatexOutputs
          mkTectonicOutputsWithInputs mkTectonicOutputsWithAdditionalInputs mkTectonicOutputs;
      };

    };

}
