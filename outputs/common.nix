builderPkg:
myself:

inputs @ { self
, nixpkgs
, utils
, ...
}:

{
  # a directory of project sub-directories that should be build in packages
  projectsDirPath ? self.outPath
  # a list of architectures of supported systems
, supportedSystems ? myself.lib.defaultSystems
}:

utils.lib.eachSystem supportedSystems (system:
  let

    defaultPackageName = "_all";
    noInputsSuffix = "_no-inputs";

    pkgs = nixpkgs.legacyPackages."${system}";
    inherit (nixpkgs) lib;

    libLocal = import ./lib.nix {
      inherit lib pkgs;
      self = myself;
    };

    builderInputs = libLocal.builderInputs builderPkg;
    builderBase = libLocal.builder builderPkg;
    builder = libLocal.builderWithInputsWrapper builderBase;

    tectonicProjectsPkgs = libLocal.tectonicProjectsPkgs builder projectsDirPath;
    tectonixProjectJoinedPkg = libLocal.tectonixProjectJoinedPkg builder defaultPackageName projectsDirPath;
    tectonixProjectJoinedPkgNoInputs = libLocal.tectonixProjectJoinedPkg builderBase defaultPackageName projectsDirPath;

  in
  rec {

    packages = tectonicProjectsPkgs // {
      "${defaultPackageName}" = tectonixProjectJoinedPkg;
      "${defaultPackageName}${noInputsSuffix}" = tectonixProjectJoinedPkgNoInputs;
    };

    # nix build
    defaultPackage = packages."${defaultPackageName}";

    # nix develop
    devShell = pkgs.mkShell rec {
      inherit (builderInputs) buildInputs nativeBuildInputs;
      shellHook = ''
        # versions
        echo "# SOFTWARE:" ${builtins.concatStringsSep ", " (map (x: x.name) ( buildInputs ++ nativeBuildInputs ))}
      '';
    };

  }
)
